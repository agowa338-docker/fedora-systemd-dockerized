FROM library/fedora:latest

RUN /usr/bin/systemctl mask dnf-makecache.timer

ENV container docker

VOLUME /run
VOLUME /sys/fs/cgroup
VOLUME /tmp

CMD /sbin/init
